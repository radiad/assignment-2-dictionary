ASM = nasm
ASMFLAGS = -f elf64
LD = ld

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

dict.o: dict.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

words.o: words.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

main.o: main.asm words.o dict.o lib.o
	$(ASM) $(ASMFLAGS) -o $@ $<

program: main.o words.o dict.o lib.o
	$(LD) -o $@ $^

clean:
	$(RM) *.o
	$(RM) program

all: program

.PHONY: clean all