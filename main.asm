%include "lib.inc"
%include "words.inc"
%include "dict.inc"

%define BUFFER_SIZE 255
%define STDERR 2

global _start

section .bss
buffer: resb BUFFER_SIZE 

section .rodata
error: db "key len must be less than 256 chars", 0
not_found: db "key not found", 0

section .text

_start:
    mov rdi, buffer
    mov rsi, BUFFER_SIZE
    call read_word

    test rax, rax
    jz .error

    mov r15, rdx ; key len

    mov rdi, buffer
    mov rsi, NEXT

    call find_word

    test rax, rax
    jz .not_found

    mov rdi, rax
    add rdi, 9
    add rdi, r15
    call print_string
    call print_newline
    call exit

.error:
    mov rdi, error
    call print_to_stderr
    call exit

.not_found:
    mov rdi, not_found
    call print_to_stderr
    call exit


print_to_stderr:
    mov rsi, rdi
    call string_length
    mov rdx, rax
    mov rdi, STDERR
    mov rax, 1
    syscall
    call print_newline
    ret