%include "lib.inc"

section .text

global find_word

find_word:
	mov r12, rdi
	mov r13, rsi

.loop:
	mov rdi, r12
	mov rsi, r13
	add rsi, 8
	call string_equals

    test rax, rax
    jnz .success

    mov r13, [r13]
    test r13, r13
    jz .fail
    jmp .loop

    .success:
        mov rax, r13
        ret

    .fail:
        xor rax, rax
        ret